/*   Activity
  
  1) Find users with letter 's' in their first name or 'd' in their last name.
		Show only the firstName and lastName fields and hide the _id field.
    
  2) Find users who are from the HR department 
  and their age is greater then or equal to 70.
  
  3) Find users with the letter 'e' in their first name 
  and has an age of less than or equal to 30. */

  // 1.)
  db.users.find(
    {$or:[
      {firstName:{
          $regex: "j",
          $options: "$i"
        }
      },
      {
        lastName: {
          $regex: "s",
          $options: "$i"
        }
      }
    ]} ,
    {
      firstName: 1,
      lastName: 1,
      _id: 0
    }
  ).pretty();

  // 2.)
  db.users.find(
    {$and:[
      {
        department: "HR"
      },
      {
        age: {$gte: 70}
      }
    ]
  }
  ).pretty();

  // 3.)
  db.users.find(
    {$and:[
      {firstName:{
        $regex: "e",
        $options: "$i"
      }
    },
      {
        age: {$lte: 30}
      }
    ]
  }
  ).pretty();